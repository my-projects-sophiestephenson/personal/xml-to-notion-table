### XML to Notion Table
When working on a research project, I searched relevant papers and used the Mendeley research library to compile my list. I use Notion for nearly everything, personal and work-related, so I wanted to transfer this list of papers to Notion so I could easily see it and work with it. Mendeley allows XML exports, so my task was to find a way to parse this XML file and move that data to my Notion table.

The two main tools I used here were [notion.py](https://github.com/jamalex/notion-py), an unofficial Python wrapper for the Notion API,  and the Python [Element Tree XML API](https://docs.python.org/3/library/xml.etree.elementtree.html). First, I parse each record in the XML file to get the information I'm interested in; then, I use notion-py to create a new row in my Notion table with that information.

You're welcome to use this repository to transfer your research library to Notion! Here are the steps you should take:
1. Clone this repo.
2. Create a file called `creds.yaml` in the repository. The file should look like this:
    ```yaml
    board_url: <the url of the your Notion table>
    token_v2: <the token_v2 cookie value of a logged-in Notion session>
    ```
3. Add your xml file to the repository with the name `papers.xml`.
4. Tweak `collect_paper_data.py` as needed to gather whatever information is relevant to you. Ensure that the table attributes being changed by `collect_paper_data.py` match the (slugified) attributes names in your Notion table.
5. Run the script to add to your table.

I'm currently getting an API error after a few papers run - I believe it is some sort of rate limiting. 
