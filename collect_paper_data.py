import xml.etree.ElementTree as ET
from notion.client import NotionClient
import yaml
import time

with open('creds.yaml') as f:
    creds = yaml.load(f, Loader=yaml.FullLoader)

# set up notion client
client = NotionClient(token_v2=creds["token_v2"])
c = client.get_block(creds["board_url"])
print(c)

# parse records
root = ET.parse('papers.xml').getroot()	
records = root.find('records')
for record in records.findall('record'):
	# get data
	title = record.find('titles').find('title').text

	authors_xml = record.find('contributors').find('authors')
	author_list = list()
	for author in authors_xml:
		author_list.append(author.text.strip())
	authors = "; ".join(author_list)

	dates_xml = record.find('dates')
	if dates_xml: year = dates_xml.find('year').text
	else: year = ""

	periodical_xml = record.find('periodical')
	if periodical_xml: periodical = periodical_xml.find('full-title').text
	else: periodical = ""

	if record.find('pages') is None: pages = ""
	else: pages = record.find('pages').text
	
	if record.find('volume') is None: volume = ""
	else: volume = record.find('volume').text

	if record.find('issue') is None: issue = ""
	else: issue = record.find('issue').text

	if record.find('publisher') is None: publisher = ""
	else: publisher = record.find('publisher').text

	if record.find('urls').find('related-urls').find('url') is None: url = ""
	else: url = record.find('urls').find('related-urls').find('url').text

	# add record to notion page
	while (True):
		try:
			row = c.collection.add_row()
			row.title = title
			row.authors = authors
			row.year = year
			row.pages = pages
			row.volume = volume
			row.issue = issue
			row.periodical = periodical
			row.publisher = publisher
			row.url = url
			break
		except:
			print('sleeping for a moment...')
			time.sleep(10)
		print(title)